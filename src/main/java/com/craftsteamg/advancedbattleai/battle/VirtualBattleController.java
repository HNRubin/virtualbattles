package com.craftsteamg.advancedbattleai.battle;

import com.craftsteamg.advancedbattleai.participant.VirtualParticipant;
import com.pixelmonmod.pixelmon.Pixelmon;
import com.pixelmonmod.pixelmon.api.events.BeatWildPixelmonEvent;
import com.pixelmonmod.pixelmon.battles.controller.BattleControllerBase;
import com.pixelmonmod.pixelmon.battles.controller.Experience;
import com.pixelmonmod.pixelmon.battles.controller.ai.BattleAIBase;
import com.pixelmonmod.pixelmon.battles.controller.participants.*;
import com.pixelmonmod.pixelmon.battles.rules.BattleRules;
import com.pixelmonmod.pixelmon.comm.packetHandlers.SwitchCamera;
import com.pixelmonmod.pixelmon.enums.battle.BattleResults;
import com.pixelmonmod.pixelmon.enums.battle.EnumBattleEndCause;
import net.minecraft.entity.player.EntityPlayerMP;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.function.BiFunction;

public class VirtualBattleController extends BattleControllerBase {

    public CountDownLatch latch;

    public VirtualBattleController(VirtualParticipant[] team1, VirtualParticipant[] team2, BattleRules rules) {
        super(team1, team2, rules);
    }

    @Override
    public HashMap<BattleParticipant, BattleResults> endBattle(EnumBattleEndCause cause, HashMap<BattleParticipant, BattleResults> results) {

        super.endBattle(cause, results);

        for (Map.Entry<BattleParticipant, BattleResults> entry : results.entrySet()) {
            if (entry.getValue() == BattleResults.VICTORY) {
                //System.out.println("Battle Ended");
                VirtualParticipant vp = (VirtualParticipant) entry.getKey();
                int x = aiResults.getOrDefault(vp.getBattleAI().getClass(), 0);
                aiResults.put(vp.getBattleAI().getClass(), ++x);
            }
        }

        if (latch != null) {
            latch.countDown();
        }

        return results;
    }




    @Override
    public void update() {
        if(battleTurn > 500)
            endBattle();

        //for(BattleParticipant bp : participants) {
        //    System.out.printf("%s has %d pokemon left\n", bp.getName().getFormattedText(), Arrays.stream(bp.allPokemon).filter(e -> e.getHealth() != 0).count());
        //}

        super.update();
        this.battleTicks = 21;

        BattleControllerBase.currentAnimations.removeIf(e -> e.bc instanceof VirtualBattleController);
    }

    @Override
    public void clearHurtTimer() {

    }

    @Override
    protected void checkPokemon() {
        for (BattleParticipant p : this.participants) {
            p.resetMoveTimer();
            if (!this.battleEnded && !p.isDefeated) {
                checkReviveSendOut(p);
                List<PixelmonWrapper> faintedPokemon = new ArrayList<>();
                for (PixelmonWrapper poke : p.controlledPokemon) {
                    if (poke.isFainted()) {

                        if (poke.newPokemonUUID == null && this.turnList.contains(poke)
                                && this.turn <= this.turnList.indexOf(poke)) {
                            this.turnList.remove(poke);
                        }

                        poke.setHealth(0);
                        p.updatePokemon(poke);
                        if (!p.hasMorePokemonReserve()) {
                            faintedPokemon.add(poke);
                            updateRemovedPokemon(poke);
                        }

                    }
                }
                for (PixelmonWrapper pw : faintedPokemon) {
                    if (!this.battleEnded) {
                        checkDefeated(p, pw);
                    }
                    if (!this.battleEnded) {
                        p.controlledPokemon.remove(pw);
                        pw.entity = null;
                    }
                }
            }
        }
    }


    public static Map<Class<? extends BattleAIBase>, Integer> aiResults = new ConcurrentHashMap<>();

    public static int timesWon(Class<? extends BattleAIBase> ai) {
        return aiResults.getOrDefault(ai, 0);
    }


}
