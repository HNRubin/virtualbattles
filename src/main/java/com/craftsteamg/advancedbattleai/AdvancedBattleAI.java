package com.craftsteamg.advancedbattleai;

import com.craftsteamg.advancedbattleai.ai.CustomAI;
import com.craftsteamg.advancedbattleai.battle.VirtualBattleController;
import com.craftsteamg.advancedbattleai.commands.SimulateBattle;
import com.craftsteamg.advancedbattleai.participant.VirtualPixelmonWrapper;
import com.pixelmonmod.pixelmon.api.attackAnimations.AttackAnimation;
import com.pixelmonmod.pixelmon.battles.controller.BattleControllerBase;
import com.pixelmonmod.pixelmon.battles.controller.ai.AdvancedAI;
import com.pixelmonmod.pixelmon.battles.controller.ai.BattleAIBase;
import com.pixelmonmod.pixelmon.battles.controller.ai.RandomAI;
import com.pixelmonmod.pixelmon.battles.controller.participants.PixelmonWrapper;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerAboutToStartEvent;
import net.minecraftforge.fml.common.event.FMLServerStartedEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;

import java.util.HashMap;
import java.util.Map;

@Mod(
        modid = "advancedbattleai",
        version = "1.0.0",
        name = "AdvancedBattleAI",
        acceptableRemoteVersions = "*",
        dependencies = "required-after:pixelmon"
)
public class AdvancedBattleAI {

    public static Map<String, Class<? extends BattleAIBase>> ai = new HashMap<>();

    @Mod.EventHandler
    private void onServerStart(FMLServerStartingEvent event) {
        event.registerServerCommand(new SimulateBattle());
        ai.put("advanced", AdvancedAI.class);
        ai.put("random", RandomAI.class);
        ai.put("custom", CustomAI.class);
    }


    public static boolean shouldContinue(AttackAnimation anim) {
        return !(anim.bc instanceof VirtualBattleController);
    }

}
