package com.craftsteamg.advancedbattleai.commands;

import com.craftsteamg.advancedbattleai.AdvancedBattleAI;
import com.craftsteamg.advancedbattleai.battle.VirtualBattleController;
import com.craftsteamg.advancedbattleai.participant.VirtualParticipant;
import com.pixelmonmod.pixelmon.Pixelmon;
import com.pixelmonmod.pixelmon.api.pokemon.Pokemon;
import com.pixelmonmod.pixelmon.api.pokemon.PokemonSpec;
import com.pixelmonmod.pixelmon.battles.BattleRegistry;
import com.pixelmonmod.pixelmon.battles.controller.BattleControllerBase;
import com.pixelmonmod.pixelmon.battles.controller.ai.BattleAIBase;
import com.pixelmonmod.pixelmon.battles.rules.BattleRules;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SimulateBattle extends CommandBase {
    @Override
    public String getName() {
        return "simulate";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return "/simulate <ai1> <ai2> <numTrials>";
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {

        Class<? extends BattleAIBase> ai1 = AdvancedBattleAI.ai.get(args[0]);
        Class<? extends BattleAIBase> ai2 = AdvancedBattleAI.ai.get(args[1]);


        ExecutorService threads = Executors.newFixedThreadPool(8);
        CountDownLatch done = new CountDownLatch(Integer.parseInt(args[2]));
        for (int i = 0; i < Integer.parseInt(args[2]); i++) {
            threads.execute(() -> {
                try {
                    VirtualParticipant p1 = new VirtualParticipant("VP1 (" + args[0] + ")", createRandomParty(), ai1);
                    VirtualParticipant p2 = new VirtualParticipant("VP2 (" + args[1] + ")", createRandomParty(), ai2);

                    VirtualBattleController bcb = new VirtualBattleController(
                            new VirtualParticipant[]{p1},
                            new VirtualParticipant[]{p2},
                            new BattleRules()
                    );

                    bcb.latch = done;

                    //p1.setBC(bcb);
                   // p2.setBC(bcb);

                    BattleRegistry.registerBattle(bcb);
                } catch (Exception e) {
                    e.printStackTrace();
                    done.countDown();
                }
            });
        }


        threads.execute(() -> {
            try {
                done.await();
                System.out.printf("AI %s won %d/%d matches against %s!\n", "VP1 (" + args[0] + ")", VirtualBattleController.timesWon(ai1), Integer.parseInt(args[2]), args[1]);
                VirtualBattleController.aiResults.clear();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });


    }

    private Pokemon[] createRandomParty() {
        Pokemon[] party = new Pokemon[6];
        for (int i = 0; i < 6; i++) {
            party[i] = Pixelmon.pokemonFactory.create(new PokemonSpec("Zoroark", "lvl:50"));
        }
        return party;
    }

}
