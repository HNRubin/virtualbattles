package com.craftsteamg.advancedbattleai.participant;

import com.pixelmonmod.pixelmon.Pixelmon;
import com.pixelmonmod.pixelmon.api.pokemon.Pokemon;
import com.pixelmonmod.pixelmon.battles.attacks.Attack;
import com.pixelmonmod.pixelmon.battles.controller.BattleControllerBase;
import com.pixelmonmod.pixelmon.battles.controller.ai.BattleAIBase;
import com.pixelmonmod.pixelmon.battles.controller.ai.MoveChoice;
import com.pixelmonmod.pixelmon.battles.controller.participants.BattleParticipant;
import com.pixelmonmod.pixelmon.battles.controller.participants.ParticipantType;
import com.pixelmonmod.pixelmon.battles.controller.participants.PixelmonWrapper;
import com.pixelmonmod.pixelmon.battles.controller.participants.TrainerParticipant;
import com.pixelmonmod.pixelmon.config.PixelmonConfig;
import com.pixelmonmod.pixelmon.config.PixelmonServerConfig;
import com.pixelmonmod.pixelmon.entities.pixelmon.abilities.Illusion;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.text.TextComponentBase;
import net.minecraft.util.text.TextComponentString;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class VirtualParticipant extends BattleParticipant {

    public Class<? extends BattleAIBase> ai;
    private final String name;
    private final Pokemon[] party;

    public VirtualParticipant(String name, Pokemon[] party, Class<? extends BattleAIBase> ai) {
        super(1);
        this.name = name;
        this.party = party;

        loadParty(Arrays.stream(party).collect(Collectors.toList()));
        this.controlledPokemon.add(allPokemon[0]);

        this.ai = ai;
    }


    @Override
    public boolean hasMorePokemonReserve() {
        return this.countAblePokemon() > this.getActiveUnfaintedPokemon().size() + this.switchingOut.size();
    }

    @Override
    public boolean canGainXP() {
        return false;
    }

    @Override
    public void endBattle() {

    }

    @Override
    public TextComponentBase getName() {
        return new TextComponentString(name);
    }

    @Override
    public MoveChoice getMove(PixelmonWrapper pixelmonWrapper) {
        return this.getBattleAI().getNextMove(pixelmonWrapper);
    }


    @Override
    public boolean checkPokemon() {
        for (Pokemon pokemon : this.party) {
            if (pokemon.getMoveset().size() == 0) {
                if (PixelmonConfig.printErrors) {
                    Pixelmon.LOGGER.info("Couldn't load Pokémon's moves.");
                }
                return false;
            }
        }
        return true;
    }

    @Override
    public void updatePokemon(PixelmonWrapper pixelmonWrapper) {
        ;
    }

    @Override
    public EntityLivingBase getEntity() {
        return new FakeEntity();
    }

    @Override
    public void updateOtherPokemon() {

    }

    @Override
    public ParticipantType getType() {
        return ParticipantType.Trainer;
    }

    @Override
    public void getNextPokemon(int i) {
        for (PixelmonWrapper pw : this.controlledPokemon) {
            if (pw.battlePosition == i) {
                this.bc.switchPokemon(pw.getPokemonUUID(), this.getBattleAI().getNextSwitch(pw), true);
                return;
            }
        }
    }

    @Override
    public PixelmonWrapper switchPokemon(PixelmonWrapper old, UUID uuid) {

        int index = this.controlledPokemon.indexOf(old);


        PixelmonWrapper newWrap = this.getPokemonFromParty(uuid);

        old.beforeSwitch(newWrap);

        newWrap.battlePosition = old.battlePosition;

        this.controlledPokemon.set(index, newWrap);

        newWrap.getBattleAbility().beforeSwitch(newWrap);
        newWrap.afterSwitch();


        return newWrap;
    }

    @Override
    public UUID getNextPokemonUUID() {
        return null; //never used
    }

    @Override
    protected void loadParty(List<Pokemon> party) {
        this.allPokemon = new PixelmonWrapper[party.size()];
        for (int i = 0; i < this.allPokemon.length; i++) {
            this.allPokemon[i] = new VirtualPixelmonWrapper(this, party.get(i), i);
        }
    }

    @Override
    public boolean getWait() {
        return false;
    }

    @Override
    public void startBattle(BattleControllerBase bc) {
        this.bc = bc;
        for (PixelmonWrapper p : this.allPokemon) {
            p.bc = bc;
            int maxHealth = p.getMaxHealth();
            int startHealth = p.getHealth();
            if (startHealth > maxHealth) {
                p.setHealth(maxHealth);
            }
            if (bc.rules.fullHeal) {
                p.setHealth(maxHealth);
                p.clearStatus();
                for (Attack attack : p.getMoveset()) {
                    if (attack != null) {
                        attack.pp = attack.getMaxPP();
                    }
                }
            }
        }
        try {
            BattleAIBase baib = ai.getDeclaredConstructor(BattleParticipant.class).newInstance(this);
            this.setBattleAI(baib);
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void animateDynamax() {

    }


}
