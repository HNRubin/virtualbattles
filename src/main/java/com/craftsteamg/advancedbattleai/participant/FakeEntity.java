package com.craftsteamg.advancedbattleai.participant;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityArmorStand;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.FMLCommonHandler;

public class FakeEntity extends EntityArmorStand {
    public FakeEntity() {
        super(FMLCommonHandler.instance().getMinecraftServerInstance().getWorld(0));
    }
}
