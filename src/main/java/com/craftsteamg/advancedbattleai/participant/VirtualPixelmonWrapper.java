package com.craftsteamg.advancedbattleai.participant;

import com.pixelmonmod.pixelmon.api.pokemon.Pokemon;
import com.pixelmonmod.pixelmon.battles.controller.participants.PixelmonWrapper;

public class VirtualPixelmonWrapper extends PixelmonWrapper {

    public VirtualPixelmonWrapper(VirtualParticipant participant, Pokemon pokemon, int partyPosition) {
        super(participant, pokemon, partyPosition);
    }

}
