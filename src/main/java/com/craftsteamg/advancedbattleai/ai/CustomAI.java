package com.craftsteamg.advancedbattleai.ai;

import com.pixelmonmod.pixelmon.RandomHelper;
import com.pixelmonmod.pixelmon.battles.attacks.Attack;
import com.pixelmonmod.pixelmon.battles.attacks.AttackBase;
import com.pixelmonmod.pixelmon.battles.controller.CalcPriority;
import com.pixelmonmod.pixelmon.battles.controller.ai.BattleAIBase;
import com.pixelmonmod.pixelmon.battles.controller.ai.MoveChoice;
import com.pixelmonmod.pixelmon.battles.controller.participants.BattleParticipant;
import com.pixelmonmod.pixelmon.battles.controller.participants.PixelmonWrapper;
import net.minecraft.util.Tuple;

import java.util.*;

public class CustomAI extends BattleAIBase {

    public CustomAI(BattleParticipant participant) {
        super(participant);
    }

    @Override
    public MoveChoice getNextMove(PixelmonWrapper pixelmonWrapper) {
        PixelmonWrapper opponent = pixelmonWrapper.bc.getOppositePokemon(pixelmonWrapper);
        ArrayList<PixelmonWrapper> targetList = new ArrayList<>(Collections.singletonList(opponent));
        Tuple<TurnPrediction, Attack> prediction = predictTurn(pixelmonWrapper, opponent);


        switch (prediction.getFirst()) {
            case KILL:
                return new MoveChoice(pixelmonWrapper, prediction.getSecond(), targetList);
            case DIE:
                //todo: oh no, gotta do something
                break;
            case NEUTRAL:
                //todo: probably the hardest part of this thing
                break;
        }


        return this.getRandomAttackChoice(pixelmonWrapper);
    }

    @Override
    public UUID getNextSwitch(PixelmonWrapper pixelmonWrapper) {
        return RandomHelper.getRandomElementFromList(this.getPossibleSwitchIDs());
    }


    private boolean hasKOMove(PixelmonWrapper user, PixelmonWrapper opponent) {
        int oppHP = opponent.getHealth();

        for (Attack attack : user.getMoveset().attacks) {
            if (attack.pp == 0)
                continue;

            int dmg = attack.doDamageCalc(user, opponent, 0);
            //todo: factor in accuracy
            if (dmg >= oppHP)
                return true;
        }

        return false;
    }

    private boolean willOutSpeedBase(PixelmonWrapper user, PixelmonWrapper opponent) {
        return CalcPriority.speedComparator.compare(user, opponent) < 0;
    }

    private int getFastestMove(PixelmonWrapper user) {
        int lowest = 0;
        for (Attack attack : user.getMoveset()) {
            int prio = attack.getMove().getPriority(user);
            if (prio > lowest)
                lowest = prio;
        }
        return lowest;
    }

    private Map<Integer, AttackPrediction> calcDamageMap(PixelmonWrapper user, PixelmonWrapper target) {
        Map<Integer, AttackPrediction> dmgMap = new HashMap<>();


        for (Attack attack : user.getMoveset().attacks) {
            if (attack == null)
                continue;
            int dmg = attack.doDamageCalc(user, target, 0);
            //todo: factor in damage range / risk
            AttackPrediction current = dmgMap.get(attack.getMove().getPriority(user));
            if (current == null || dmg >= current.dmg)
                dmgMap.put(attack.getMove().getPriority(user), new AttackPrediction(attack, dmg));
        }


        return dmgMap;
    }


    private Tuple<TurnPrediction, Attack> predictTurn(PixelmonWrapper user, PixelmonWrapper target) {
        boolean userOutspeeds = willOutSpeedBase(user, target);

        int userHealth = user.getHealth();
        int targetHealth = target.getHealth();

        Map<Integer, AttackPrediction> dmgMapUser = calcDamageMap(user, target);
        Map<Integer, AttackPrediction> dmgMapTarget = calcDamageMap(target, user);

        for (int i = 6; i >= -6; i--) {
            AttackPrediction atkMaxDmgUser = dmgMapUser.get(i);
            AttackPrediction atkMaxDmgTarget = dmgMapTarget.get(i);

            int maxDmgUser = atkMaxDmgUser == null ? 0 : atkMaxDmgUser.dmg;
            int maxDmgTarget = atkMaxDmgTarget == null ? 0 : atkMaxDmgTarget.dmg;


            if (!userOutspeeds && maxDmgTarget >= userHealth)
                return new Tuple<>(TurnPrediction.DIE, atkMaxDmgTarget.atk); //Can't kill if we're dead!

            if (maxDmgUser >= targetHealth)
                return new Tuple<>(TurnPrediction.KILL, atkMaxDmgUser.atk);
        }

        return new Tuple<>(TurnPrediction.NEUTRAL, null);
    }

    private UUID optimalSwitch(AttackBase opponentAttack, PixelmonWrapper user, PixelmonWrapper opponent) {
        for (PixelmonWrapper wrapper : user.getParticipant().allPokemon) {
            if (wrapper == null || wrapper.getPokemonUUID().equals(user.getPokemonUUID()) || wrapper.getHealth() <= 0) {
                continue;
            }


        }

        return null;
    }

    private class AttackPrediction {


        public AttackPrediction(Attack atk, int dmg) {
            this.atk = atk;
            this.dmg = dmg;
        }

        private Attack atk;
        private int dmg;
    }

    private enum TurnPrediction {
        KILL,
        DIE,
        NEUTRAL
    }

}
