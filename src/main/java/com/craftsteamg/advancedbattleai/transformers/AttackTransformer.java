package com.craftsteamg.advancedbattleai.transformers;

import net.minecraft.launchwrapper.IClassTransformer;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.tree.*;

import static org.objectweb.asm.Opcodes.*;

public class AttackTransformer implements IClassTransformer {
    @Override
    public byte[] transform(String name, String transformedName, byte[] basicClass) {
        if (!transformedName.equals("com.pixelmonmod.pixelmon.api.attackAnimations.AttackAnimation"))
            return basicClass;

        ClassNode classNode = new ClassNode();
        ClassReader classReader = new ClassReader(basicClass);
        classReader.accept(classNode, 0);

        for (MethodNode method : classNode.methods) {
            if (method.name.equals("sendBattleEffect")) {
                InsnList insn = new InsnList();
                LabelNode jmp = new LabelNode();
                insn.add(new VarInsnNode(ALOAD, 0));
                insn.add(new MethodInsnNode(INVOKESTATIC, "com/craftsteamg/advancedbattleai/AdvancedBattleAI", "shouldContinue", "(Lcom/pixelmonmod/pixelmon/api/attackAnimations/AttackAnimation;)Z", false));
                insn.add(new JumpInsnNode(IFNE, jmp));
                insn.add(new InsnNode(RETURN));
                insn.add(jmp);

                method.instructions.insert(method.instructions.getFirst(), insn);
            }
        }

        ClassWriter classWriter = new SafeClassWriter(classReader, getClass().getClassLoader(), ClassWriter.COMPUTE_FRAMES);
        classNode.accept(classWriter);

        return classWriter.toByteArray();
    }
}
