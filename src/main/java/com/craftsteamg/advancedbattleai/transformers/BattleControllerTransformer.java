package com.craftsteamg.advancedbattleai.transformers;


import net.minecraft.launchwrapper.IClassTransformer;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.util.ListIterator;

import static org.objectweb.asm.Opcodes.*;

public class BattleControllerTransformer implements IClassTransformer {
    @Override
    public byte[] transform(String name, String transformedName, byte[] basicClass) {
        if (!transformedName.equals("com.pixelmonmod.pixelmon.battles.controller.BattleControllerBase"))
            return basicClass;

        ClassNode classNode = new ClassNode();
        ClassReader classReader = new ClassReader(basicClass);
        classReader.accept(classNode, 0);

        for (MethodNode method : classNode.methods) {

            ListIterator<AbstractInsnNode> iterator = method.instructions.iterator();

            while (iterator.hasNext()) {
                AbstractInsnNode node = iterator.next();

                if (node.getOpcode() == INVOKESPECIAL) {
                    MethodInsnNode methodNode = (MethodInsnNode) node;
                    if (methodNode.name.equals("checkPokemon")) {
                        methodNode.setOpcode(INVOKEVIRTUAL);
                    }
                }

            }
        }

        ClassWriter classWriter = new ClassWriter(0);
        classNode.accept(classWriter);

        return classWriter.toByteArray();
    }
}
