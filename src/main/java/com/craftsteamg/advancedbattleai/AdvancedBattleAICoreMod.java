package com.craftsteamg.advancedbattleai;

import com.craftsteamg.advancedbattleai.transformers.AttackTransformer;
import com.craftsteamg.advancedbattleai.transformers.BattleControllerTransformer;
import net.minecraftforge.fml.relauncher.IFMLLoadingPlugin;

import javax.annotation.Nullable;
import java.util.Map;

@IFMLLoadingPlugin.TransformerExclusions("com.craftsteamg.advancedbattleai.transformers")
@IFMLLoadingPlugin.MCVersion("1.12.2")
public class AdvancedBattleAICoreMod implements IFMLLoadingPlugin {

    @Override
    public String[] getASMTransformerClass() {
        return new String[]{
                BattleControllerTransformer.class.getName(),
                AttackTransformer.class.getName()
        };
    }

    @Override
    public String getModContainerClass() {
        return null;
    }

    @Nullable
    @Override
    public String getSetupClass() {
        return null;
    }

    @Override
    public void injectData(Map<String, Object> data) {

    }

    @Override
    public String getAccessTransformerClass() {
        return null;
    }
}
