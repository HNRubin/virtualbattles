package org.pokecentral.abilities;

import com.craftsteamg.ezclassreplacer.interfaces.InjectNode;
import com.craftsteamg.ezclassreplacer.interfaces.ReplaceClass;
import com.pixelmonmod.pixelmon.battles.controller.participants.PixelmonWrapper;
import com.pixelmonmod.pixelmon.entities.pixelmon.helpers.EvolutionQuery;
import com.pixelmonmod.pixelmon.enums.EnumSpecies;
import com.pixelmonmod.pixelmon.enums.forms.EnumZygarde;

@ReplaceClass(
        target = "com.pixelmonmod.pixelmon.entities.pixelmon.abilities.PowerConstruct",
        partial = true
)
public class NewPowerConstruct {

    @InjectNode
    public void applyRepeatedEffect(PixelmonWrapper pokemon) {
        if (!pokemon.bc.simulateMode && pokemon.getSpecies() == EnumSpecies.Zygarde && pokemon.getFormEnum() != EnumZygarde.COMPLETE && pokemon.getHealthPercent() <= 50.0F) {
            pokemon.bc.sendToAll("pixelmon.abilities.powerconstruct.activate");
            pokemon.pokemon.getPersistentData().setInteger("SrcForm", pokemon.getForm());
            if (pokemon.entity != null)
                pokemon.evolution = new EvolutionQuery(pokemon.entity, EnumZygarde.COMPLETE.getForm());
            pokemon.setForm(EnumZygarde.COMPLETE);
            if (pokemon.entity != null)
                pokemon.bc.updateFormChange(pokemon.entity);
            pokemon.bc.sendToAll("pixelmon.abilities.powerconstruct.transform", pokemon.getNickname());
        }
    }
}
