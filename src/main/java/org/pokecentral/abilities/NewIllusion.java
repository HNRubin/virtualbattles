package org.pokecentral.abilities;

import com.craftsteamg.ezclassreplacer.interfaces.InjectNode;
import com.craftsteamg.ezclassreplacer.interfaces.ReplaceClass;
import com.pixelmonmod.pixelmon.battles.controller.participants.BattleParticipant;
import com.pixelmonmod.pixelmon.battles.controller.participants.PixelmonWrapper;
import com.pixelmonmod.pixelmon.battles.controller.participants.PlayerParticipant;
import com.pixelmonmod.pixelmon.battles.controller.participants.WildPixelmonParticipant;
import com.pixelmonmod.pixelmon.entities.pixelmon.stats.Gender;
import com.pixelmonmod.pixelmon.enums.EnumSpecies;

@ReplaceClass(
        target = "com.pixelmonmod.pixelmon.entities.pixelmon.abilities.Illusion",
        partial = true
)
public class NewIllusion {

    public EnumSpecies disguisedPokemon = null;
    public String disguisedNickname = null;
    public String disguisedTexture = null;
    public Gender disguisedGender = null;
    public int disguisedForm = -1;

    @InjectNode
    public void beforeSwitch(PixelmonWrapper newPokemon) {
        if (newPokemon.bc.simulateMode) {
            return;
        }
        BattleParticipant participant = newPokemon.getParticipant();
        if (participant instanceof WildPixelmonParticipant) {
            return;
        }

        PixelmonWrapper disguised = null;
        for (int i = participant.allPokemon.length - 1; i >= 0; i--) {
            PixelmonWrapper pw = participant.allPokemon[i];
            if (!pw.isFainted()) {
                if (!pw.getPokemonUUID().equals(newPokemon.getPokemonUUID())) {
                    disguised = pw;

                    this.disguisedTexture = disguised.getRealTextureNoCheck();
                    if (this.disguisedTexture.equals(newPokemon.getRealTextureNoCheck())) {
                        this.disguisedTexture = null;
                        return;
                    }
                    this.disguisedPokemon = disguised.getSpecies();
                    this.disguisedNickname = disguised.getNickname();
                    this.disguisedGender = disguised.getGender();
                    this.disguisedForm = disguised.getForm();
                    break;
                } else {
                    return;
                }
            }
        }
        if (disguised == null) {
            return;
        }
        if (newPokemon.entity != null)
            newPokemon.entity.transformServer(disguised.getSpecies(), this.disguisedForm, this.disguisedTexture);
    }


    @InjectNode
    public void applySwitchInEffect(PixelmonWrapper newPokemon) {
        if (newPokemon.entity != null && newPokemon.entity.transformedTexture == null && this.disguisedPokemon != null) {
            newPokemon.entity.transformServer(this.disguisedPokemon, this.disguisedForm, this.disguisedTexture);
        }
    }


    @InjectNode
    private void fade(PixelmonWrapper target) {
        if (this.disguisedPokemon != null && !target.bc.simulateMode) {
            this.disguisedPokemon = null;
            this.disguisedNickname = null;
            this.disguisedTexture = null;
            this.disguisedGender = null;
            target.bc.sendToAll("pixelmon.abilities.illusion", target.getNickname());
            if (target.entity != null)
                target.entity.cancelTransform();

            target.bc.participants.stream().filter((participant) -> {
                return participant instanceof PlayerParticipant;
            }).forEach((participant) -> {
                ((PlayerParticipant) participant).updateOpponentPokemon();
            });
            if (target.getPlayerOwner() != null) {
                this.updateOwner(target);
            }
        }
    }

    private void updateOwner(PixelmonWrapper pokemon) {

    }

}
