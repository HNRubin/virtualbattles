package org.pokecentral.abilities;

import com.craftsteamg.ezclassreplacer.interfaces.InjectNode;
import com.craftsteamg.ezclassreplacer.interfaces.ReplaceClass;
import com.pixelmonmod.pixelmon.battles.attacks.Attack;
import com.pixelmonmod.pixelmon.battles.controller.participants.PixelmonWrapper;
import com.pixelmonmod.pixelmon.battles.status.StatusType;
import com.pixelmonmod.pixelmon.battles.status.Transformed;
import com.pixelmonmod.pixelmon.entities.pixelmon.EntityPixelmon;
import com.pixelmonmod.pixelmon.entities.pixelmon.abilities.Illusion;
import com.pixelmonmod.pixelmon.entities.pixelmon.stats.Moveset;
import net.minecraft.network.datasync.EntityDataManager;

@ReplaceClass(
        target = "com.pixelmonmod.pixelmon.entities.pixelmon.abilities.Imposter",
        partial = true
)
public class NewImposter {
    @InjectNode
    public void applySwitchInEffect(PixelmonWrapper newPokemon) {
        if (newPokemon.bc.simulateMode) {
            return;
        }
        PixelmonWrapper target = newPokemon.bc.getOppositePokemon(newPokemon);
        if (target.getBattleAbility() instanceof Illusion && ( (Illusion) target.getBattleAbility() ).disguisedPokemon != null
                || target.hasStatus(StatusType.Substitute, StatusType.Transformed)) {
            return;
        }

        newPokemon.bc.sendToAll("pixelmon.abilities.imposter", newPokemon.getNickname(),
                target.getNickname());
        if(newPokemon.entity != null) {
            EntityDataManager dataManager = newPokemon.entity.getDataManager();
            dataManager.set(EntityPixelmon.dwTransformation, -1);
        }

        newPokemon.addStatus(new Transformed(newPokemon, target), target);

        Moveset tempMoveset = new Moveset().withPokemon(newPokemon.pokemon);
        for (Attack a : target.getMoveset())
        {
            if (a != null)
            {
                Attack copy = a.copy();
                copy.pp = 5;
                copy.overridePPMax(5);
                tempMoveset.add(copy);
            }
        }
        newPokemon.setTemporaryMoveset(tempMoveset);
    }
}
