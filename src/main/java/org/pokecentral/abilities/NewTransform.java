package org.pokecentral.abilities;

import com.craftsteamg.ezclassreplacer.interfaces.InjectNode;
import com.craftsteamg.ezclassreplacer.interfaces.ReplaceClass;
import com.pixelmonmod.pixelmon.battles.attacks.Attack;
import com.pixelmonmod.pixelmon.battles.controller.log.AttackResult;
import com.pixelmonmod.pixelmon.battles.controller.participants.PixelmonWrapper;
import com.pixelmonmod.pixelmon.battles.status.StatusType;
import com.pixelmonmod.pixelmon.battles.status.Transformed;
import com.pixelmonmod.pixelmon.entities.pixelmon.EntityPixelmon;
import com.pixelmonmod.pixelmon.entities.pixelmon.abilities.AbilityBase;
import com.pixelmonmod.pixelmon.entities.pixelmon.abilities.Illusion;
import com.pixelmonmod.pixelmon.entities.pixelmon.stats.Moveset;
import net.minecraft.network.datasync.EntityDataManager;

@ReplaceClass(
        target = "com.pixelmonmod.pixelmon.battles.attacks.specialAttacks.basic.Transform",
        partial = true
)
public class NewTransform {

    @InjectNode
    public AttackResult applyEffectDuring(PixelmonWrapper user, PixelmonWrapper target) {
        AbilityBase targetAbility = target.getBattleAbility();
        if ((targetAbility instanceof Illusion && ((Illusion) targetAbility).disguisedPokemon != null)
                || target.hasStatus(StatusType.Substitute, StatusType.Transformed)) {
            user.bc.sendToAll("pixelmon.effect.effectfailed");
            return AttackResult.failed;
        }
        if (!user.bc.simulateMode) {

            if(user.entity != null) {
                EntityDataManager dataManager = user.entity.getDataManager();
                if (user.removeStatus(StatusType.Transformed)) {
                    dataManager.set(EntityPixelmon.dwTransformation, 0);
                }
                dataManager.set(EntityPixelmon.dwTransformation, -1);
            }

            user.bc.sendToAll("pixelmon.status.transform", user.getNickname(), target.getNickname());
            user.addStatus(new Transformed(user, target), target);

            Moveset tempMoveset = new Moveset().withPokemon(user.pokemon);
            for (Attack a : target.getMoveset()) {
                if (a != null) {
                    Attack copy = a.copy();
                    copy.pp = 5;
                    copy.overridePPMax(5);
                    tempMoveset.add(copy);
                }
            }
            user.setTemporaryMoveset(tempMoveset);
        }

        return AttackResult.succeeded;
    }
}
