package org.pokecentral.other;

import com.craftsteamg.ezclassreplacer.interfaces.InjectNode;
import com.craftsteamg.ezclassreplacer.interfaces.ReplaceClass;
import com.pixelmonmod.pixelmon.battles.controller.participants.PixelmonWrapper;

@ReplaceClass(
        target = "com.pixelmonmod.pixelmon.battles.status.Transformed",
        partial = true
)
public class NewTransformed {

    @InjectNode
    public void applyBeforeEffect(PixelmonWrapper user, PixelmonWrapper target) {
        if (user.entity != null)
            user.entity.transformServer(target.getSpecies(), target.getForm(), target.entity.getTextureNoCheck().toString());
    }

    @InjectNode
    public void applyEndOfBattleEffect(PixelmonWrapper pokemon) {
        if (pokemon.entity != null)
            pokemon.entity.cancelTransform();
    }
}
