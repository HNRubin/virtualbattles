package org.pokecentral.other;

import com.craftsteamg.ezclassreplacer.interfaces.InjectNode;
import com.craftsteamg.ezclassreplacer.interfaces.ReplaceClass;
import com.pixelmonmod.pixelmon.battles.controller.participants.PixelmonWrapper;
import com.pixelmonmod.pixelmon.battles.status.Freeze;
import com.pixelmonmod.pixelmon.battles.status.Sunny;
import com.pixelmonmod.pixelmon.comm.ChatHandler;
import com.pixelmonmod.pixelmon.enums.EnumSpecies;
import com.pixelmonmod.pixelmon.enums.EnumType;
import com.pixelmonmod.pixelmon.enums.forms.EnumShaymin;
import net.minecraft.util.text.TextComponentTranslation;

@ReplaceClass(
        target = "com.pixelmonmod.pixelmon.battles.status.Freeze",
        partial = true
)
public class NewFreeze {

    @InjectNode
    public static boolean freeze(PixelmonWrapper user, PixelmonWrapper target) {
        if (target.hasType(EnumType.Ice)) {
            return false;
        } else if (target.bc.globalStatusController.getWeather() instanceof Sunny) {
            return false;
        } else {
            TextComponentTranslation message = ChatHandler.getMessage("pixelmon.effect.frozesolid", target.getNickname());
            if (target.getSpecies() == EnumSpecies.Shaymin && target.getForm() == 1) {
                target.setForm(EnumShaymin.LAND);
                user.bc.sendToAll("pixelmon.abilities.changeform", target.getNickname());
            }

            return target.addStatus(new Freeze(), user, message);
        }
    }
}
